syntax on
set cursorline
hi CursorLine term=none cterm=none ctermbg=black
set number
set softtabstop=2
set tabstop=2
set shiftwidth=2
set smartindent
